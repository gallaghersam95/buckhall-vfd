import { Component } from '@angular/core';
import { ApplicationsService } from './applications.service';
import { Application } from './application';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'app';

  private sidebarShowing: Boolean = true;
  private data: Observable<any[]>;

  constructor(private applicationService: ApplicationsService) {
  	this.data = this.applicationService.getAll();
  }

  barsClick() {
  	this.sidebarShowing = !this.sidebarShowing;
  }
  
  sortBy(attr) {
  	switch(attr) {
  	
  		case 'status':
  			this.applicationService.sortByStatus();
  			break;
  			
  		case 'appliedOn':
  			this.applicationService.sortByAppliedOn();
  			break;
  		
  		default:
  			break; 
  	
  	}
  }

}
