export interface Application {
	name: string,
	appliedOn: string,
	email: string,
	phone: string
}
