import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';

import { ApplicationsService } from './applications.service';
import { environment } from '../environments/environment';

import { AppComponent } from './app.component';
import { ApplicationComponent } from './application/application.component';


@NgModule({
  declarations: [
    AppComponent,
    ApplicationComponent,
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    BsDropdownModule.forRoot(),
    ModalModule.forRoot()
  ],
  providers: [
  	ApplicationsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
