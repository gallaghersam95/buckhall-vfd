import { Component, OnInit, Input, TemplateRef  } from '@angular/core';
import { DatePipe } from '@angular/common';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
  selector: 'app-application',
  templateUrl: './application.component.html',
  styleUrls: ['./application.component.css']
})
export class ApplicationComponent implements OnInit {

	@Input() application;
	modalRef: BsModalRef;

	constructor(private modalService: BsModalService) {}

	openModal(template: TemplateRef<any>) {
		console.log("YES");
		this.modalRef = this.modalService.show(template);
	}

	ngOnInit() {
	}

}
