/*
	Pull data from Firebase datastore
*/

import { Injectable } from '@angular/core';
import { Application } from './application';
import { AngularFirestore } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class ApplicationsService {

	public data: Subject<any[]>;

  constructor(private db: AngularFirestore) {

		this.data = db.collection('applications');

  }

  getAll() {
  	return this.data.valueChanges();;
  }
  
  sortByStatus() {
  	// Something
  }
  
  sortByAppliedOn() {
  
  	this.data = this.db.orderBy('appliedOn'));
  
  }

}
