// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDPUMDqmt264_3emEqPb26WAhsZz2ECF5A",
    authDomain: "membership-applications.firebaseapp.com",
    databaseURL: "https://membership-applications.firebaseio.com",
    projectId: "membership-applications",
    storageBucket: "membership-applications.appspot.com",
    messagingSenderId: "55380538076"  
  }
};
